MEMORY
{
/* NOTE K = KiBi = 1024 bytes */
/* FLASH : ORIGIN = 0x00000000, LENGTH = 1024K */
FLASH : ORIGIN = 0x0000C000, LENGTH = 976K
RAM : ORIGIN = 0x10000000, LENGTH = 384K
}

/* This is where the call stack will be allocated. */
/* The stack is of the full descending type. */
/* NOTE Do NOT modify `_stack_start` unless you know what you are doing */
_stack_start = ORIGIN(RAM) + LENGTH(RAM) - 8;

/* Advanced users can place the stack inthe CCRAM */
/* which is smaller but faster. */
/* _stack_start = ORIGIN(CCRAM) + LENGTH(CCRAM); */
