//! Initialization code

#![no_std]

// pub use stm32f3_discovery::{leds::Leds, stm32f3xx_hal, switch_hal};
pub use switch_hal::{ActiveHigh, IntoSwitch, OutputSwitch, Switch, ToggleableOutputSwitch};

pub use panic_itm; // panic handler

pub use cortex_m_rt::entry;

use ambiq_apollo3_pac::{clkgen::clkkey, CACHECTRL, CLKGEN, GPIO, MCUCTRL, PWRCTRL};

pub fn init() {
    let (gpio, cachectrl, clkgen) = unsafe { (&*GPIO::ptr(), &*CACHECTRL::ptr(), &*CLKGEN::ptr()) };
    let pwrctrl = unsafe { &*PWRCTRL::ptr() };
    let mcuctrl = unsafe { &*MCUCTRL::ptr() };

    clkgen.clkkey.write(|w| w.clkkey().key());
    clkgen.cctrl.write(|w| w.coresel().hfrc());
    clkgen.clkkey.write(|w| {
        *w = clkkey::W::reset_value();
        w
    });

    cachectrl
        .cachecfg
        .modify(|_, w| w.dcache_enable().clear_bit().icache_enable().clear_bit());
    cachectrl.cachecfg.write(|w| {
        w.enable()
            .clear_bit()
            .cache_clkgate()
            .set_bit()
            .cache_ls()
            .clear_bit()
            .data_clkgate()
            .set_bit()
            .enable_monitor()
            .clear_bit()
            .lru()
            .clear_bit()
            .config()
            .w1_128b_1024e()
            .dcache_enable()
            .set_bit()
            .icache_enable()
            .set_bit()
    });
    cachectrl.cachecfg.modify(|_, w| w.enable().set_bit());

    if mcuctrl.chiprev.read().revmaj().is_a()
        && mcuctrl.chiprev.read().revmin().is_rev1()
        && pwrctrl.supplystatus.read().simobuckon().is_on()
    {
        pwrctrl.devpwren.modify(|_, w| w.pwrpdm().en());
    }

    mcuctrl
        .simobuck4
        .modify(|r, w| unsafe { w.bits(r.bits() | 1 << 24) });

    gpio.encb
        .write(|w| unsafe { w.encb().bits(1 << 14 | 1 << 5 | 1 << 15 | 1 << 12) });

    gpio.padkey.write(|w| w.padkey().key());

    gpio.padregj
        .write(|w| w.pad37fncsel().gpio37().pad37strng().low());

    gpio.padregl.write(|w| {
        w.pad46fncsel()
            .gpio46()
            .pad46strng()
            .low()
            .pad47fncsel()
            .gpio47()
            .pad47strng()
            .low()
            .pad44fncsel()
            .gpio44()
            .pad44strng()
            .low()
    });

    gpio.cfge.write(|w| w.gpio37outcfg().pushpull());
    gpio.cfgf.write(|w| {
        w.gpio46outcfg()
            .pushpull()
            .gpio47outcfg()
            .pushpull()
            .gpio44outcfg()
            .pushpull()
    });

    gpio.padkey.write(|w| unsafe { w.bits(0) });

    gpio.wtsb
        .write(|w| unsafe { w.bits(1 << 14 | 1 << 5 | 1 << 15 | 1 << 12) });
}
