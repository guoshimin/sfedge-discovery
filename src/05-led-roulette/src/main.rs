#![deny(unsafe_code)]
#![no_main]
#![no_std]

// use aux5::{Delay, DelayMs, LedArray, OutputSwitch, entry};
use aux5::entry;

#[entry]
fn main() -> ! {
    // let (mut delay, mut leds): (Delay, LedArray) = aux5::init();
    let _y;
    let x = 42;
    _y = x;
    aux5::init();

    // let ms = 50_u8;
    // // infinite loop; just so we don't leave this stack frame
    loop {
        // for curr in 0..4 {
        //     let next = (curr + 1) % 4;
        //     leds[next].on().ok();
        //     delay.delay_ms(ms);
        //     leds[curr].off().ok();
        //     delay.delay_ms(ms);
        // }
    }
}
